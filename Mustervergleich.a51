;+-----------------------------------------------------------------------------+
;| mustervergleich.a51                             Christoph Haefner, 26.07.10 |
;+-----------------------------------------------------------------------------+
;| Beschreibung:                                                               |
;|  Mikrocontroller-Projekt "Muster vergleich":                                |
;|  Es soll an den beiden 7-Segment-Anzeigen jeweils ein "Muster"              |
;|  angezeigt werden.                                                          |
;|  Der User entscheidet dann ob die beiden Muster gleich sind.                |
;|  Das ganze laeuft 10 mal hintereinander ab.                                 |
;|  Und am Ende erfolgt eine Ausgabe der prozentualen Erfolgsquote.            |
;|                                                                             |
;|  Eingangssignale:                                                           |
;|   - beide Taster (P3.2 + P3.3)                                              |
;|                                                                             |
;|  Ausgangssignale:                                                           |
;|   - beide 7-Segment-Anzeigen (P2 + P0)                                      |
;|                                                                             |
;|  Funktionen:                                                                |
;|   - Timer                                                                   |
;+-----------------------------------------------------------------------------+
;|  Tastenbelegung:                                                            |
;|  Frage: Waren die angezeigten Muster gleich?                                |
;|   - P3.3 => NEIN / P3.2 => JA                                               |
;+-----------------------------------------------------------------------------+
;| Benutze Timer                                                               |
;|   - T0 => 16bit ohne Auto-Reload => Zufallszahlen "erzeugen"                |
;|   - T1 => 16bit ohne Auto-Reload => fuer "warten"-Unterprogramme            |
;+-----------------------------------------------------------------------------+
;| Benutzte Register                                                           |
;|   - R0 => unbenutzt                                                         |
;|   - R1 => Anzahl richtiger Antworten                                        |
;|   - R2 =>   -- wieder frei --                                               |
;|   - R3 => Wurden gleiche Muster angezeigt oder nicht? (Gleiche Muster = 1)  |
;|   - R4 => Zufallszahlzwischenspeicher (bis zum Tastendruck)                 |
;|   - R5 => Zufallszahlzwischenspeicher (Tastendruckl�nge)                    |
;|   - R6 => Anzahl der Spielrunden                                            |
;|   - R7 => Entprellwarteschleife                                             |
;+-----------------------------------------------------------------------------+

include reg51.inc

;+-----------------------------------------------------------------------------+
;| Startwerte setzen                                                           |
;+-----------------------------------------------------------------------------+
MOV A,  #0
MOV R1, #0
MOV R5, #0
MOV R6, #10d ; => Spielrunden
MOV R7, #0
MOV P2, #0 ; untere LED-Reihe aus
MOV P0, #0 ; obere LED-Reihe aus
MOV DPTR, #tabelle

;+-----------------------------------------------------------------------------+
;| Timer 0&1 einrichten                                                        |
;+-----------------------------------------------------------------------------+
MOV TMOD, #00010001b ; Timer0: Modus1 = 16-Bit ohne Auto-Reload
							; Timer1: Modus1 = 16-Bit ohne Auto-Reload

;+-----------------------------------------------------------------------------+
;| Direkt Timer 0 starten, um Dauer bis zum ersten Tastendruck zu messen       |
;+-----------------------------------------------------------------------------+
MOV TL0, #0 ; Timer0 Low-Byte leeren
MOV TH0, #0 ; Timer0 High-Byte leeren
SETB TCON.4 ; Timer0 starten
                     
;+-----------------------------------------------------------------------------+
;| Ersten Tastendruck abfangen um erste Zufallszahl zu ermitteln               |
;| Danach wird die Tastendrucklaenge beim Entscheiden fuer die weiteren        |
;| Zufallszahlen genutzt                                                       |
;+-----------------------------------------------------------------------------+
start:
JNB P3.3, start
LCALL entprell
MOV R4, TH0 ; Zufallszahl speichern
schleife2:
JB P3.3, schleife2
LCALL entprell
MOV R5, TH0 ; Zufallszahl speichern

;+-----------------------------------------------------------------------------+
;| Spielablauf starten / "spiel"-Marke setzen                                  |
;+-----------------------------------------------------------------------------+
spiel:
	;+--------------------------------------------------------------------------+
	;| Auf jeden Fall die Zeit bis zum Tastendruck als 1. Muster anzeigen       |
	;| Anzeigedauer eine Wartezeit                                              |
	;+--------------------------------------------------------------------------+
	MOV P2, R4
	lcall warten
	MOV P2, #0 ; P2 wieder leeren
	
	;+--------------------------------------------------------------------------+
	;| 2 Wartezeiten warten                                                     |
	;+--------------------------------------------------------------------------+
	lcall warten
	lcall warten
	
	;+--------------------------------------------------------------------------+
	;| 2. Muster anzeigen                                                       |
	;| Anzeigedauer eine Wartezeit                                             |
	;+--------------------------------------------------------------------------+
		;+-----------------------------------------------------------------------+
		;| Entscheiden ob zwei gleiche Muster angezeigt werden sollen            |
		;+-----------------------------------------------------------------------+		
		MOV ACC, R5
		JB ACC.0, verschiedene
		; Es werden die gleichen Muster angezeigt
		MOV P0, R4
		MOV R3, #1
		LJMP angezeigt
		verschiedene:
		; Es werden verschiedene Muster angezeigt
		MOV P0, R5
		MOV R3, #0
	angezeigt:
	lcall warten
	MOV P0, #0 ; P0 wieder leeren
	
   ;+--------------------------------------------------------------------------+
	;| Auf Entscheidung des Nutzers warten                                      |
	;+--------------------------------------------------------------------------+
	entscheidung:
		JB P3.2, falsch
		JB P3.3, richtig
	LJMP entscheidung
	
	;+--------------------------------------------------------------------------+
	;| Entscheidungs auswerten (Auf die Frage: Sind die Muster gleich gewesen?) |
	;| R3 speichert ob gleiche Muster angezeigt wurden oder verschiedene        |
	;| R3 ist 1 wenn gleiche Angezeigt wurden und 0 wenn verschiedene           |
	;+--------------------------------------------------------------------------+
	richtig:
	MOV A, R3 ; Information ob gleiche Muster angezeigt wurden in Akku schreiben um per JZ/JNZ Abfragen zu k�nnen
	lcall entprell
	MOV R4, TH0 ; Zufallszahl speichern
	richtig_entprell_schleife:
	JB P3.3, richtig_entprell_schleife
	lcall entprell
	MOV R5, TH0 ;  Zufallszahl speichern
	JZ r_verschiedene ; Springe, wenn Akkuinhalt gleich Null
	LJMP rundenende
	r_verschiedene:
	INC R1
	LJMP rundenende
	
	falsch:
	MOV A, R3 ; Information ob gleiche Muster angezeigt wurden in Akku schreiben um per JZ/JNZ Abfragen zu k�nnen
	lcall entprell
	MOV R4, TH0 ; Zufallszahl speichern
	falsch_entprell_schleife:
	JB P3.2, falsch_entprell_schleife
	lcall entprell
	MOV R5, TH0 ;  Zufallszahl speichern
	JNZ f_gleiche ; Springe, wenn Akkuinhalt gleich Null
	LJMP rundenende
	f_gleiche:
	INC R1

rundenende:
;+-----------------------------------------------------------------------------+
;| Eine Warzeit lang warten, damit das n�chste Muster nicht direkt             |
;| angezeigt wird                                                              |
;+-----------------------------------------------------------------------------+
lcall warten

;+-----------------------------------------------------------------------------+
;| N�chste Runde :)                                                            |
;+-----------------------------------------------------------------------------+
DJNZ R6, spiel
;/*
; * Auswertung
; * Spezialfall zu nutze machen, dass bei 10 Runden die Anzahl der
; * richtigen Treffer (R1) gleich der Prozentzahl ist. Ausnahme 10
; */
CJNE R1, #10d, ungleich_zehn
; EQUAL
MOV P2, #00010011b
MOV P0, #01111111b
LJMP ende
ungleich_zehn:
; NOT EQUAL
MOV A, R1
MOVC A, @A+DPTR
MOV P2, A
	MOV A, #0d
	MOVC A, @A+DPTR
	MOV P0, A
ende:
LJMP ende
 
;+-----------------------------------------------------------------------------+
;| Entprellwarteschleife / Dauer: 0,002048s                                    |
;+-----------------------------------------------------------------------------+
entprell:
NOP
NOP
NOP
NOP
NOP
NOP
DJNZ R7, entprell
ret

;+-----------------------------------------------------------------------------+
;| Warteunterprogramm mit einer Wartezeit die �ber P1 eingestellt wird         |
;+-----------------------------------------------------------------------------+
warten:
MOV R2, P1 ; Laenge ist von P1 abh�ngik
MOV TL1, #0 ; Timer1 auf 0 setzen
MOV TH1, #0 ; Timer1 auf 0 setzen
SETB TCON.6 ; Timer1 starten
; POLLING
warten1_marke:
JNB TCON.7, warten1_marke
CLR TCON.7 ; Overflowbit l�schen
DJNZ R2, warten1_marke
; /POLLING
CLR TCON.6 ; Timer1 stopen
ret

tabelle:
db 01111110b, 00010010b, 10111100b, 10110110b, 11010010b, 11100110b, 11101110b, 00110010b, 11111110b, 11110010b
end
;+------------------------------mustervergleich.a51----------------------------+ 
